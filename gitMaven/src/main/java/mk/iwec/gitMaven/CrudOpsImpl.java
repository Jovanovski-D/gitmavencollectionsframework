package mk.iwec.gitMaven;

import java.util.HashMap;
import java.util.Map;

public class CrudOpsImpl implements CrudOps {

	// private List<Map<Integer, Student>> ml = new ArrayList<>();

	Map<Integer, Student> stdmap = new HashMap<Integer, Student>();

	@Override
	public void add(Student student) {

		stdmap.put(student.getId(), student);
		// ml.add(stdmap);
	}

	@Override
	public void update(int id, Student student) {

		for (Map.Entry<Integer, Student> entry : stdmap.entrySet()) {
			if (id == entry.getKey()) {
				entry.setValue(student);
				break;
			}
		}
	}

	@Override
	public void delete(int id) {
		for (Map.Entry<Integer, Student> entry : stdmap.entrySet()) {
			if (id == entry.getKey()) {
				stdmap.remove(entry.getKey());
				break;
			}
		}
	}

	@Override
	public void read() {
		System.out.println(stdmap);
	}
}
