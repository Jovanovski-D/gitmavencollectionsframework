package mk.iwec.gitMaven;

public interface CrudOps {

	public void add(Student student);

	public void update(int id, Student student);

	public void delete(int id);

	public void read();
}
