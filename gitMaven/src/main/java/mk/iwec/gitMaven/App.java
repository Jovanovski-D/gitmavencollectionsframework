package mk.iwec.gitMaven;

public class App {

	public static void main(String[] args) {

		CrudOps co = new CrudOpsImpl();

		co.add(new Student("Vele", "Velev", 10));
		co.add(new Student("Pero", "Perov", 11));
		co.add(new Student("Krume", "Krumev", 12));
		co.read();
		
		System.out.println("\nUpdating values for the student with key 10");
		co.update(10, new Student("Darko", "Jovanovski", 10));
		co.read();
		
		System.out.println("\nDeleting values for student with key 10");
		co.delete(10);
		co.read();
	}

}
